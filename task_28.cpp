#include <iostream>
using namespace std;


int main()
{
	int year, days;
	bool a = 1;
	while (a == 1)
	{
		cout << "Input number of year: ";
		cin >> year;
		if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
		{
			days = 366;
		}	
		else
		{
			days = 365;
		}
		cout << "Number of days this year = " << days << endl;
	}
	
	
	return 0;


}