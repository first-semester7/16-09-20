#include <iostream>
using namespace std;
int main()
{
	int x, y;
	cout << "Input sign of x: ";
	cin >> x;
	if (x < 0)
	{
		y = 0;
	}	
	else if (x % 2 == 0)
	{
		y = 1;
	}
	else if (x % 2 != 0)
	{
		y = -1;
	}	
	cout << "Sign of y = " << y;
	return 0;

}

