#include <iostream>
using namespace std;

int main()
{
	float x, y;
	cout << "Input sign of x: ";
	cin >> x;
	if (x <= 0)
	{
		y = -x;
	}	
	else if (x > 0 && x < 2)
	{
		y = pow(x, 2);
	}
	else if (x >= 2)
	{
		y = 4;
	}
	cout << "Sign of y = " << y;
	return 0;
}